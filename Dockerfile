FROM quay.io/centos/centos:stream9

# Install the Red Hat certificate authority and the packages needed for
# development on CentOS Stream and RHEL
COPY 2022-IT-Root-CA.pem /etc/pki/ca-trust/source/anchors/
COPY 2015-IT-Root-CA.pem /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust extract

RUN echo "fastestmirror=True" >> /etc/dnf/dnf.conf && \
    yum -y install epel-release && \
    yum -y install \
           python3-GitPython \
           python3-pip \
           python3-pyyaml \
           python3-requests && \
    yum clean all

# Copy the config file for gitbz check
COPY gitbz.json /etc/gitbz.json

# Install the check_gitbz tool
COPY pyproject.toml /check_gitbz/pyproject.toml
COPY check_gitbz /check_gitbz/check_gitbz
RUN pip install /check_gitbz

