#!/usr/bin/python3

import json
import logging
import os
import re
import requests
import sys
import yaml


import git as gitpython
from . import utils as gitbz

# Phase Identifiers
# Phase 230 is "Planning / Development / Testing" (AKA DevTestDoc)
# Phase 450 is "Stabilization"
phase_devtestdoc = 230
phase_stabilization = 450


# PROJECT_OVERRIDE is for components that do not match between CentOS Stream
# and RHEL dist-git, most notably the components that contained '+' characters
# in their names. We *MUST* be sure that distrobaker is configured to redirect
# the component syncs properly before adding an entry to this list

PROJECT_OVERRIDE = {
    "dvdplusrw-tools": "dvd+rw-tools",
    "centos-release": "redhat-release",
    "compat-sap-cplusplus": "compat-sap-c++",
    "compat-sap-cplusplus-10": "compat-sap-c++-10",
    "compat-sap-cplusplus-11": "compat-sap-c++-11",
    "compat-sap-cplusplus-12": "compat-sap-c++-12",
    "compat-sap-cplusplus-13": "compat-sap-c++-13",
    "libsigcplusplus20": "libsigc++20",
    "memtest86plus": "memtest86+",
    "perl-Text-TabsplusWrap": "perl-Text-Tabs+Wrap",
}


class CheckGitBZException(Exception):
    pass


def check_gitbz(query_url, git_repo, namespace, project, oldrev, newrev, refname):
    logger = logging.getLogger(__name__)
    repo_commits = gitbz.get_commits(git_repo, oldrev, newrev)
    commits_data = []
    fixed = []
    for repo_commit in repo_commits:
        speclines = gitbz.specAdditions(project, repo_commit)
        message = gitbz.get_commit_message(repo_commit)

        logger.debug("gitbz data for %s: %s", repo_commit.hexsha, message + speclines)

        single_commit = {
            "hexsha": repo_commit.hexsha,
            "files": gitbz.get_commit_files(repo_commit),
            "resolved": gitbz.bugzillaIDs("Resolves?:", message + speclines),
            "related": gitbz.bugzillaIDs("Related?:", message + speclines),
            "reverted": gitbz.bugzillaIDs("Reverts?:", message + speclines),
        }
        fixed.extend(gitbz.bugzillaIDs("Fixes?:", message + speclines))
        commits_data.append(single_commit)

    request_data = {
        "package": project,
        "namespace": namespace,
        "ref": refname,
        "commits": commits_data,
    }

    logger.debug("Sending data to gitbz API to verify hooks result ...")
    logger.debug("Data sent: %s", json.dumps(request_data, sort_keys=True, indent=2))

    res = requests.post(query_url, json=request_data, timeout=905)

    logger.debug("Response from gitbz API: %s", res.text)
    try:
        res.raise_for_status()
    except requests.exceptions.RequestException as e:
        logger.warning("Failed gitbz check for request: %s", request_data)
        logger.exception(e)
        raise CheckGitBZException("Invalid response from gitbz webservice API")

    payload = json.loads(res.text)
    if len(fixed) > 0:
        # These are bugs that were identified by the "Fixes:" prefix rather
        # than the "Resolves:" prefix.
        payload["logs"] += "\n"
        payload[
            "logs"
        ] += "*** WARNING: Detected one or more uses of 'Fixes:' instead of 'Resolves:'\n"
        payload["logs"] += "  Unverified:\n"
        for bug in fixed:
            payload["logs"] += "    {}\n".format(bug)

    return payload


def read_config(config_file):
    with open(config_file, "r") as f:
        c = json.load(f)
    return c


def _datesplit(isodate):
    date_string_tuple = isodate.split("-")
    return [int(x) for x in date_string_tuple]


def parse_rhel_shortname(shortname):
    # The shortname is in the form rhel-9-1.0 or rhel-10.0[.beta]
    m = re.match(
        "rhel-(?P<major>[0-9]+)[.-](?P<minor>[0-9]+)([.]0|[.](?P<extra>.*))?", shortname
    )
    if not m:
        raise RuntimeError("Could not parse version from {}".format(shortname))

    major_version = int(m.group("major"))
    minor_version = int(m.group("minor"))
    extra_version = m.group("extra") or None

    return major_version, minor_version, extra_version


def determine_active_y_version(rhel_version, api_url):
    """
    Returns: A 4-tuple containing:
      0. The major release version(int)
      1. The active Y-stream version(int)
      2. Whether the active release is the pre-X.0 beta
      3. Whether we are in the Exception Phase(bool)
    """
    logger = logging.getLogger(__name__)

    # Query the "package pages" API for the current active Y-stream release
    request_params = {
        "phase__in": "{},{}".format(phase_devtestdoc, phase_stabilization),
        "product__shortname": "rhel",
        "relgroup__shortname": rhel_version,
        "format": "json",
    }

    res = requests.get(
        os.path.join(api_url, "latest", "releases"),
        params=request_params,
        timeout=60,
    )
    res.raise_for_status()
    payload = json.loads(res.text)
    logger.debug("Response from PP API: {}".format(json.dumps(payload, indent=2)))
    if len(payload) < 1:
        # Received zero potential release matches
        logger.warning("Didn't match any active releases. Assuming pre-Beta.")

        # Fake up a Beta payload
        payload = [
            {
                "shortname": "{}.0.beta".format(rhel_version),
                "phase": phase_devtestdoc,
            }
        ]

    active_y_version = -1
    beta = False
    for entry in payload:
        shortname = entry["shortname"]

        # The shortname is in the form rhel-9-1.0 or rhel-10.0[.beta]
        # Extract the active Y-stream version
        x_version, y_version, extra_version = parse_rhel_shortname(shortname)

        if y_version > active_y_version:
            active_y_version = y_version
            beta = bool(extra_version and "beta" in extra_version)

        in_exception_phase = entry["phase"] == 450

    logger.debug(
        "Active Y-stream: {}, Enforcing: {}".format(
            active_y_version, in_exception_phase
        )
    )

    return x_version, active_y_version, beta, in_exception_phase


# Certain packages are not synced to RHEL, and will always use the 'cXs' branch
# rules. This list is maintained in the distrobaker configuration:
def get_unsynced_projects(cfg):
    res = requests.get(
        os.path.join(cfg["distrobaker_config"]),
        timeout=60,
    )
    res.raise_for_status()
    payload = yaml.safe_load(res.content.decode("utf-8"))
    return payload["configuration"]["control"]["exclude"]["rpms"]


def determine_refname(cfg, namespace, project, target_branch):
    logger = logging.getLogger(__name__)

    # Identify the RHELevant version
    try:
        rhel_version = cfg["mapping"][target_branch]
    except KeyError:
        # If it's not listed in the mapping, just return the target
        # branch. Most likely it is a stream branch.
        # Always enforce in this case
        return target_branch, True

    # Look up the Y-1 branch name
    x_version, active_y_version, is_beta, in_exception_phase = (
        determine_active_y_version(rhel_version, cfg["api_url"])
    )

    # Always enforce in exception phase
    enforced = in_exception_phase

    if project in get_unsynced_projects(cfg):
        return target_branch, enforced

    # Special case: there will never be a prior branch for an X.0 beta
    if is_beta:
        return target_branch, enforced

    if x_version >= 10 and active_y_version <= 0:
        # For 10.0 and later X.0 releases, check for a rhel-X.0-beta branch
        divergent_branch = "rhel-{}.0-beta".format(x_version)
    elif x_version <= 9:
        divergent_branch = "rhel-{}.{}.0".format(x_version, active_y_version - 1)
    else:
        # Starting with RHEL 10, the branch names have dropped the extra .0
        divergent_branch = "rhel-{}.{}".format(x_version, active_y_version - 1)

    logger.debug("Divergent branch: {}".format(divergent_branch))

    # The rhel-9.0.0 branch is a special case, it's unsupported
    # Always enforce on this branch
    if rhel_version == "rhel-9" and divergent_branch == "rhel-9.0.0":
        return target_branch, True

    # Determine if the divergent branch exists for this repo
    g = gitpython.cmd.Git()
    try:
        g.ls_remote(
            "--exit-code",
            os.path.join(cfg["rhel_dist_git"], namespace, project),
            divergent_branch,
        )
        branch_exists = True
    except gitpython.GitCommandError as e:
        t, v, tb = sys.exc_info()
        # `git ls-remote --exit-code` returns "2" if it cannot find the ref
        if e.status == 2:
            branch_exists = False
        else:
            raise

    # If the branch exists, then it means we've diverged and should use the
    # original Y-stream branch rules for gitbz checks
    if branch_exists:
        logger.info(
            "\x1b[43mBranch {0} exists in RHEL dist-git. Using rules from {1}\x1b[0m".format(
                divergent_branch, target_branch
            )
        )
        return target_branch, enforced
    else:
        # If it does not exist, then use the Y-1 branch rules for gitbz checks
        logger.info(
            "\x1b[43mBranch {0} does not exist in RHEL dist-git. Using rules from {0}\x1b[0m".format(
                divergent_branch
            )
        )
        # Gitbz checks are always enforced on Z-stream branches
        return divergent_branch, True


def log_setup():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Log all messages into the debug log
    debug_handler = logging.FileHandler(
        "{}/debug.log".format(os.environ["CI_PROJECT_DIR"])
    )
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)

    # Also log everything INFO and higher to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger


def main():
    logger = log_setup()
    cfg = read_config("/etc/gitbz.json")

    logger.debug("Working Directory: {}".format(os.getcwd()))

    namespace = os.environ["CI_PROJECT_NAMESPACE"].rsplit("/")[-1]
    centos_git_repo = gitpython.Repo(path=os.environ["CI_PROJECT_DIR"])

    project = os.environ["CI_PROJECT_NAME"]
    if project in PROJECT_OVERRIDE:
        project = PROJECT_OVERRIDE[project]

    refname, enforced = determine_refname(
        cfg=cfg,
        namespace=namespace,
        project=project,
        target_branch=os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"],
    )

    # In the rare case where a merge request includes no file changes,
    # Gitlab does not set the $CI_MERGE_REQUEST_DIFF_BASE_SHA value.
    # In this situation, we'll treat the oldrev as the immediate
    # ancestor of the newrev.
    newrev = os.environ["CI_COMMIT_SHA"]
    oldrev = os.environ.get("CI_MERGE_REQUEST_DIFF_BASE_SHA", f"{newrev}^")

    result = check_gitbz(
        query_url=cfg["gitbz_query_url"],
        git_repo=centos_git_repo,
        namespace=namespace,
        project=project,
        oldrev=oldrev,
        newrev=newrev,
        refname="refs/heads/{}".format(refname),
    )

    logger.info(result["logs"])
    if result["result"] != "ok":
        logger.warning(result["error"])
        if enforced:
            logger.debug("Enforcing mode: deny this merge request")
            sys.exit(1)
        else:
            # We specifically treat exit(77) as a nonfatal failure
            logger.debug("Non-enforcing: permit this with warnings")
            sys.exit(77)


if __name__ == "__main__":
    main()
